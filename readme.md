# IntervalArray

An array which handles intervals.
For example:

	var intervalArray = new IntervalArray();
	// add two intervals
	intervalArray.push({from: 12, to: 14}, {from: 14, to: 18});
	

## License
MIT License