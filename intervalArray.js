var IntervalArray = (function() {
    var constr,
        sort = function() {
            this.intervals.sort(function(a, b) {
                return a["from"] > b["from"];
            });
            return this;
        },
        get = function(index) {
            return this.intervals[index];
        },
        copy = function() {
            var originals = this.intervals,
                c = [];
            for(var i = 0, length = originals.length; i < length; i++) {
                var item = originals[i];
                c.push({from: item.from, to: item.to, scale: item.scale});
            }
            return c;
        },
        search = (function() {
            var startIndex,
                stopIndex,
                binarySearch = function(items, value, func) {
                    
                    var stop = stopIndex,
                        middle = Math.floor((stopIndex + startIndex)/2),
                        currentValue;
                    
                    while(startIndex < stopIndex) {
                        currentValue = func.call(items[middle]);
                        if(value == currentValue) {
                            break;
                        } else if (value < currentValue){
                            stopIndex = middle - 1;
                        } else {
                            startIndex = middle + 1;
                        }

                        //recalculate middle
                        middle = Math.floor((stopIndex + startIndex)/2);
                    }
                    if(middle < 0) {
                        middle = 0;
                    } else if(middle > stop) {
                        middle = stop;
                    }
                        
                    return middle;
                };

            return function(from, to) {
                var len = this.length;

                if(len == 0)
                    return {count: 0, side: "right"};

                var intervals = this.intervals,
                    lastIndex = len - 1;

                // if the interval is on the right side, return
                if(from >= intervals[lastIndex]["to"]) {
                    return {count: 0, side: "right"};
                }
                // if the interval is on the left side, return
                if(to <= intervals[0]["from"]) {
                    return {count: 0, side: "left"};
                }

                var start,
                    startValue,
                    firstComplete = false;

                if(from <= intervals[0]["from"]) {
                    start = 0;
                    startValue = intervals[0]["from"];
                    firstComplete = true;
                } else {
                    // set indices
                    startIndex = 0;
                    stopIndex = lastIndex;

                    start = binarySearch(intervals, from, function() {return this["from"]});
                    while(from < intervals[start]["from"] && start > 0) {
                        --start;
                    }
                    if(from >= intervals[start]["to"]) {
                        ++start;
                        startValue = intervals[start]["from"];
                    } else {
                        startValue = from;
                    }

                    if(from == intervals[start]["from"])
                        firstComplete = true;
                }

                var stop,
                    stopValue,
                    lastComplete = false;

                if(to >= intervals[lastIndex]["to"]) {
                    stop = lastIndex;
                    stopValue = intervals[lastIndex]["to"];
                    lastComplete = true;
                } else {
                    // set indices
                    // the start index is bigger or equal the start:
                    startIndex = start;
                    stopIndex = lastIndex;
                    stop = binarySearch(intervals, to, function() {return this["to"]});
                    while(to > intervals[stop]["to"] && stop < lastIndex) {
                        ++stop;
                    }
                    if(to <= intervals[stop]["from"]) {
                        --stop;
                        stopValue = intervals[stop]["to"];
                    } else {
                        stopValue = to;
                    }

                    if(from == intervals[stop]["to"])
                        lastComplete = true;
                }

                if(start > stop) {
                    return {count: 0, side: "middle", position: start};
                } else if(start == stop) {
                    if(startValue >= stopValue)
                        return {count: 0, side: "middle", position: start};
                    firstComplete = lastComplete = (firstComplete && lastComplete);
                }

                return {
                    count: stop - start + 1, 
                    startIndex: start,
                    startValue: startValue,
                    stopIndex: stop,
                    stopValue: stopValue,
                    firstComplete: firstComplete,
                    lastComplete: lastComplete
                };
            };

        }()),
        push = function(item) {
            var intervals = this.intervals;
            for(var i = 0, length = arguments.length; i < length; i++) {
                add.call(this, arguments[i]);
                this.length = intervals.length;
            }
            return this.length;
        },
        pop = function() {
            var intervals = this.intervals,
                element = intervals.pop();
                
            this.length = intervals.length;
            return element;
        },
        shift = function() {
            var intervals = this.intervals,
                element = intervals.shift();
        
            this.length = intervals.length;
            return element;
        },
        cutSides = function(intervals, from, to, start, stop) {
            var interval = intervals[start];
            if(from > interval.from) {
                intervals.splice(start, 0, {from: interval.from, to: from, scale: interval.scale});
                interval.from = from;
                ++start;
                ++stop;
            } else if(from < interval.from) {
                intervals.splice(start, 0, {from: from, to: interval.from, scale: -1});
                ++stop;
            }

            interval = intervals[stop-1];
            if(to < interval.to) {
                intervals.splice(stop, 0, {from: to, to: interval.to, scale: interval.scale});
                interval.to = to;
            } else if(to > interval.to) {
                intervals.splice(stop, 0, {from: interval.to, to: to, scale: -1});
                ++stop;
            }
            return [start, stop];
        }
        add = function(item) {
            item.scale = item.scale || 0;

            var from = item.from,
                to = item.to,
                scale = item.scale;

            if(from == to)
                throw Error("'from' equals 'to'!");
            
            if(from > to) {
                item.from = to;
                item.to = from;
                from = item.from;
                to = item.to;
            }

            var intervals = this.intervals,
                len = intervals.length;
            
            if(len == 0) {
                return intervals.push(item);
            }
            
            var intervalInfo = search.call(this, item.from, item.to);
        
            if(intervalInfo.count) {
                var start = intervalInfo.startIndex,
                    i,
                    stop = start + intervalInfo.count,
                    lastInterval,
                    interval,
                    chain = [],
                    startStop = cutSides(intervals, from, to, start, stop);
                    
                start = startStop[0];
                stop = startStop[1];

                // set start variable
                i = start;

                // push first to chain and begin with loop
                lastInterval = intervals[start];
                if(lastInterval.scale <= scale) {
                    chain.push(lastInterval);
                }
                ++i;

                var buildChain = function() {
                    var chainLength = chain.length;
                    if(chainLength) {
                        intervals.splice(i-chainLength, chainLength, {
                            from: intervals[i-chainLength].from, 
                            to: lastInterval.to,
                            scale: scale
                        });

                        i -= chainLength-1;
                        stop -= chainLength-1;
                        chain = [];
                    }
                };

                for(; i < stop; i++) {
                    interval = intervals[i];
                    var hasBetterScale = (interval.scale > scale);

                    if(lastInterval.to != interval.from) {
                        lastInterval = {from: lastInterval.to, to: interval.from, scale: -1};
                        intervals.splice(i, 0, lastInterval);
                        ++i;
                        ++stop;
                        chain.push(lastInterval);
                    }

                    if(hasBetterScale) {
                        buildChain();
                    } else {
                        chain.push(interval);
                    }

                    lastInterval = interval;
                }

                // build chain for the last time
                buildChain();

                // look on the sides
                if(start) {
                    lastInterval = intervals[start-1];
                    interval = intervals[start];
                    if(lastInterval.scale == interval.scale && lastInterval.to == interval.from) {
                        intervals.splice(start, 1);
                        lastInterval.to = interval.to;
                        --stop;
                    }
                }
                if(stop < intervals.length) {
                    lastInterval = intervals[stop-1];
                    interval = intervals[stop];
                    if(lastInterval.scale == interval.scale && lastInterval.to == interval.from) {
                        intervals.splice(stop, 1);
                        lastInterval.to = interval.to;
                        --stop;
                    }
                }
            } else {
                // insert item in the gap
                switch(intervalInfo.side) {
                    case("left"):
                        if(intervals[0].from == item.to && intervals[0].scale == item.scale) {
                            intervals[0].from = item.from;
                        } else {
                            intervals.unshift(item);
                        }
                        break;
                    case("right"):
                        if(intervals[len-1].to == item.from && intervals[len-1].scale == item.scale) {
                            intervals[len-1].to = item.to;
                        } else {
                            intervals.push(item);
                        }
                        break;
                    case("middle"):
                        var pos = intervalInfo.position,
                            merges = 0;
                    
                        if(intervals[pos].from == item.to && intervals[pos].scale == item.scale) {
                            intervals[pos].from = item.from;
                            merges++;
                        }
                        if(intervals[pos-1].to == item.from && intervals[pos-1].scale == item.scale) {
                            intervals[pos-1].to = item.to;
                            merges++;
                        }
                        
                        if(merges == 2) {
                            intervals[pos-1].to = intervals[pos].to;
                            intervals.splice(pos, 1);
                        } else if(merges == 0) {
                            intervals.splice(pos, 0, item);
                        }
                        break;
                }
            }
        },
        worse = function(from, to, scale) {
            return find.call(this, from, to, scale, function(s1, s2) {return s1 < s2}, [{from: from, to: to}]);
        },
        find = function(from, to, scale, compareFunction, emptyReturnValue) {
            scale = scale || 0;

            if(from == to)
                throw Error("'from' equals 'to'!");
            
            if(from > to) {
                var tempFrom = from;
                from = to;
                to = tempFrom;
            }
            
            var intervals = this.copy(),
                intervalInfo = search.call(this, from, to);
            if(intervalInfo.count) {
                var start = intervalInfo.startIndex,
                    i,
                    stop = start + intervalInfo.count,
                    lastInterval,
                    interval,
                    chain = [],
                    result = [],
                    startStop = cutSides(intervals, from, to, start, stop);
                
                start = startStop[0];
                stop = startStop[1];
                
                // set start variable
                i = start;

                // push first to chain and begin with loop
                lastInterval = intervals[start];
                if(compareFunction(lastInterval.scale, scale)) {
                    chain.push(lastInterval);
                }
                ++i;
                
                var buildChain = function() {
                    var chainLength = chain.length;
                    if(chainLength) {
                        result.push({
                            from: chain[0]["from"],
                            to: chain[chainLength-1]["to"]
                        });
                        chain = [];
                    }
                };
                
                for(; i < stop; i++) {
                    interval = intervals[i];

                    if(lastInterval.to != interval.from) {
                        lastInterval = {from: lastInterval.to, to: interval.from, scale: -1};
                        intervals.splice(i, 0, lastInterval);
                        ++i;
                        ++stop;
                        chain.push(lastInterval);
                    }

                    if(!compareFunction(interval.scale, scale)) {
                        buildChain();
                    } else {
                        chain.push(interval);
                    }

                    lastInterval = interval;
                }
                
                // build chain for the last time
                buildChain();
                
                return result;
                
            } else {
                return emptyReturnValue;
            }
        },
        available = function(from, to, scale) {
            return find.call(this, from, to, scale, function(s1, s2) {return s1 >= s2;}, []);
        },
        clear = function() {
            this.intervals = [];
            this.length = 0;
            return this;
        };

    constr = function() {
        this.intervals = [];
        this.length = 0;
    };

    constr.prototype = {
        constructor: IntervalArray,
        get: get,
        push: push,
        pop: pop,
        shift: shift,
        search: search,
        worse: worse,
        available: available,
        copy: copy,
        clear: clear
    };

    return constr;
}());